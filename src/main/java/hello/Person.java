package hello;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.List;

public class Person {

	@Id private String id;

	private String firstName;
	private String lastName;

	private String[] arrayProperty; //works
	private List<Email> listProperty; //works

	@DBRef private List<Email> linkedListProperty; //does not work, should be same like listProperty


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String[] getArrayProperty() {
		return arrayProperty;
	}

	public void setArrayProperty(String[] arrayProperty) {
		this.arrayProperty = arrayProperty;
	}

	public List<Email> getListProperty() {
		return listProperty;
	}

	public void setListProperty(List<Email> listProperty) {
		this.listProperty = listProperty;
	}

	public List<Email> getLinkedListProperty() {
		return linkedListProperty;
	}

	public void setLinkedListProperty(List<Email> linkedListProperty) {
		this.linkedListProperty = linkedListProperty;
	}
}
