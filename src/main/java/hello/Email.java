package hello;

import org.springframework.data.annotation.Id;

public class Email {

    @Id private String id;
    private String adress;

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }
}
